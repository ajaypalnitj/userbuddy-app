Template.forgotPassword.onRendered(function() {
	$('#forgot-form').validate({
		rules: {	
			email: {
				required: true,
				email: true
			}
		}, 
		messages: {
			email: {
				required: "Please enter your email address to register.",
				email: "Please enter a valid email address."
			}
		},
		submitHandler() {
			const email = $('[name=email]').val();
			
			Accounts.forgotPassword({ email: email }, function(error) {
				if(error) {
					Bert.alert( error.reason, 'danger', 'growl-top-right' );
				} else {
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Check your inbox to reset your password.' });
					FlowRouter.go("/login");
				}
			});
		}
	});
});

Template.forgotPassword.events({
	'submit form': function( event ) {
		event.preventDefault();
	}
});