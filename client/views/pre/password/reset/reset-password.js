Template.resetPassword.onRendered(function() {
	$('#reset-form').validate({
		rules: {	
			newPassword: {
				required: true
			},
			confirmNewPassword: {
				required: true
			}
		}, 
		messages: {
			newPassword: {
				required: "Please enter new password"
			},
			confirmNewPassword: {
				required: "Please confirm your password"
			}
		},
		submitHandler() {
			const newPassword = $('[name=newPassword]').val(),
			confirmNewPassword = $('[name=confirmNewPassword]').val(),
			token = FlowRouter.current().params.resetToken;

			if(newPassword != confirmNewPassword) {
				document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Passwords are not matching' });
			} else if(newPassword === confirmNewPassword) {
				Accounts.resetPassword(token, newPassword, (error) => {
					if(error) {
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: error.reason });
					} else {
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Password reset successful, Login to continue' });
					}
				});
			}
		}
	});
});

Template.resetPassword.events({
	'submit form': function( event ) {
		event.preventDefault();
	}
});