Template.verifyPhone.onRendered(function() {
	$('#otp-form').validate({
		rules: {	
			otp: {
				required: true
			}
		}, 
		messages: {
			otp: {
				required: "Please enter one time password."
			}
		},
		submitHandler() {
			const otp = $('[name=otp]').val(),
			urlUserPhone = FlowRouter.getQueryParam('userPhone'),
			urlUserEmail = FlowRouter.getQueryParam('email'),
			encodeuUrlUserPhone = encodeURIComponent(FlowRouter.getQueryParam('userPhone')).replace(/%20/g, "+"),
			sessionUserPhone = Session.get('userPhone'),
			sessionUserEmail = Session.get('userEmail');

			if (_.isEqual(encodeuUrlUserPhone, sessionUserPhone) && _.isEqual(urlUserEmail, sessionUserEmail)) {
				userPhone = sessionUserPhone;
				userEmail = sessionUserEmail;

				Meteor.call('verify', userPhone, otp, function(verError, verResponse){
					if (verError) {
						Bert.alert( verError.reason, 'danger', 'growl-top-right' );
					} else {
						if(verResponse === true) {
							Bert.alert( 'Login to continue.', 'info', 'growl-top-right' );
							FlowRouter.go(FlowRouter.path("login"));
						} else {
							Bert.alert( 'Verification error', 'danger', 'growl-top-right' );
						}
					}
				});

				Session.clear('userPhone');
				Session.clear('userEmail');
			} else {
				Bert.alert( 'Incorrect Email address or Phone Number', 'danger', 'growl-top-right' );
			}
		}
	});
});

Template.verifyPhone.events({
	'submit form': function( event, template ) {
		event.preventDefault();
	},
	'click .close': function() {
		document.querySelector('dialog').close();
	}
});