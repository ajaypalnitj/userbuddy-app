Template.progressBar.helpers({
	progress: function () {
		if(Session.get('uploadResponse') == 100) {
			$('#nprogress').remove();
		}
		return Session.get('uploadResponse');
	}
});