Template.userAddress.helpers({
	address: function () {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.address ) {
			return Meteor.user().userDetails.address;
		}
	}
});

Template.userAddress.events({
	'click .show-addAddress-modal': function () {
		var dialog = document.querySelector('dialog#addAddress');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	}
});