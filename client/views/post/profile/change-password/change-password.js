Template.changePassword.onRendered(function() {
	$('#password-form').validate({
		rules: {
			oldPassword: {
				required: true
			},	
			newPassword: {
				required: true
			},
			confirmNewPassword: {
				required: true
			}
		}, 
		messages: {
			oldPassword: {
				required: "Old Password is required.",
			},
			newPassword: {
				required: "New Password is required."
			},
			confirmNewPassword: {
				required: "Confirm New Password is required."
			}
		},
		submitHandler() {
			const oldPassword = $('[name="oldPassword"]').val(),
			newPassword = $('[name="newPassword"]').val(),
			confirmNewPassword = $('[name="confirmNewPassword"]').val();

			if (_.isEqual(newPassword, confirmNewPassword)) {
				Accounts.changePassword(oldPassword, newPassword, function (error) { 
					if(error) {
						Bert.alert( error.reason, 'danger', 'growl-top-right' );
					} else {
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Password changed.' });
					}
				});
				document.getElementById('password-form').reset();
			} else {
				Bert.alert( 'Passwords not matching', 'danger', 'growl-top-right' );
			}
		}
	});
});

Template.changePassword.events({
	'submit form': function(event) {
		event.preventDefault();
	}
});