Template.record.onRendered(function() {
	var self = this;
	self.autorun(function() {
		self.subscribe('records');  
	});
});

Template.record.onCreated(function() {
	Session.set('uploadResponse', '');
	Session.set('uploadPreview', '');
	Session.set('url', '');
});

Template.record.helpers({
	record: function() {
		return Uploads.find({ user: Meteor.userId() }).count();
	}
});

Template.record.events({
	'click #options': function( event ) {
		$('.btn-fab').toggleClass('active');
	}
});