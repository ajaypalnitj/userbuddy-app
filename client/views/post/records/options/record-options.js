Template.options.events({
	'click #photo': function( event, template ) {
		event.preventDefault();
		$('.btn-fab').toggleClass('active');
		var dialog = document.querySelector('dialog#modal-image');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	},
	'click #value': function( event, template ) {
		event.preventDefault();
		$('.btn-fab').toggleClass('active');
		var dialogValue = document.querySelector('dialog#modal-value');
		if (! dialogValue.show) {
			dialogPolyfill.registerDialog(dialogValue);
		}
		dialogValue.show();
	}
});