Template.record.onRendered(function() {
	this.subscribe('reminders');
});

Template.reminders.helpers({
	reminders: function() {
		return Reminders.find({ user: Meteor.userId()}).fetch();
	}
});

Template.reminders.events({
	'click .reminder .footer-btn': function( event, template ) {
		$('.record-full').fadeIn();
	},
	'click #close': function( event, template ) {
		$('.record-full').fadeOut();
	},
	'click .appointment-header': function( event ) {
		if ($(event.currentTarget).hasClass('active')) {
			$(event.currentTarget.nextElementSibling).stop().slideUp();
			$(event.currentTarget).removeClass('active');
		} else {
			$('.appointment-header').removeClass('active');
			$('.app-drop').slideUp();
			$(event.currentTarget.nextElementSibling).stop().slideToggle();
			$(event.currentTarget).addClass('active');
		}
	}
});