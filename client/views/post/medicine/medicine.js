Template.medicine.onRendered(function() {
	this.subscribe('medicines');
});

Template.medicine.helpers({
	medis: function() {
		return Medicines.find({ user: Meteor.userId() }).fetch();
	}
});

Template.medicine.events({
	'click #order, keypress': function( event, template ) {
		if ((event.type === 'click') || (event.type === 'keypress' && event.which === 13) ) {
			event.preventDefault();
		}
	},
	'click .refresh': function( event, template ) {
		event.preventDefault();
		Session.set('orderId', '');
		Session.set('orderId', $(event.currentTarget).attr('order-id'));
		var dialog = document.querySelector('dialog#orderDetails');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	},
	'click .show-medicineOrder-modal': function( event ) {
		event.preventDefault();
		var dialog = document.querySelector('dialog#medicineOrder');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	}
});