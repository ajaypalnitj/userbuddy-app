Chats = new Mongo.Collection('chats');
Messages = new Mongo.Collection('messages');

Meteor.methods({
	newChat: function () {

		var chat = {
			userIds: this.userId,
			createdAt: new Date()
		};

		return Chats.insert(chat);
	},

	removeChat: function (chatId) {
		check(chatId, String);

		Messages.remove({ chatId: chatId });
		return Chats.remove({ _id: chatId });
	},

	welcomeMessage: function (message) {
		check(message, {
			text: String,
			type: String,
			chatId: String
		});

		message.timestamp = new Date();

		var messageId = Messages.insert(message);
		Chats.update(message.chatId, { $set: { lastMessage: message } });
		return messageId;
	},

	newMessage: function (message) {
		check(message, {
			text: String,
			type: String,
			chatId: String
		});

		message.timestamp = new Date();
		message.userId = this.userId;

		var messageId = Messages.insert(message);
		Chats.update(message.chatId, { $set: { lastMessage: message } });
		return messageId;
	},

	newImgMessage: function (message) {
		check(message, {
			image: String,
			type: String,
			chatId: String
		});

		message.timestamp = new Date();
		message.userId = this.userId;

		var messageId = Messages.insert(message);
		Chats.update(message.chatId, { $set: { lastMessage: message } });
		return messageId;
	}
});