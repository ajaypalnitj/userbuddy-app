Meteor.startup(function () {
	process.env.MAIL_URL = 'smtp://localhost:25/';
	
	// var username = 'care@carebuddy.co';
	// var password = 'CareBuddy@13';
	// process.env.MAIL_URL = 'smtp://'+username+':'+password+':@smtp.zoho.com:465/';

	Accounts.emailTemplates.siteName = 'CareBuddy';
	Accounts.emailTemplates.from = 'CareBuddy <care@carebuddy.co>';

	SSR.compileTemplate( 'welcomeEmail', Assets.getText( 'welcome.html' ) );
	SSR.compileTemplate( 'forgotPasswordEmail', Assets.getText( 'forgotPassword.html' ) );

	Accounts.emailTemplates.verifyEmail = {
		subject(user) {
			return "Welcome to Carebuddy";
		},
		html(user, url) {
			var emailData = {
				user: user,
				url: url.replace( '#/', '' )
			};
			return SSR.render( 'welcomeEmail', emailData );
		}
	};

	Accounts.emailTemplates.resetPassword = {
		subject(user) {
			return "CareBuddy - Reset Password";
		},
		html(user, url) {
			var emailData = {
				user: user,
				url: url.replace( '#/', '' )
			};
			return SSR.render( 'forgotPasswordEmail', emailData );
		}
	};

	Accounts.config({
		sendVerificationEmail: true,
		loginExpirationInDays: null
	});

	Accounts._options = { 
		verificationCodeLength: 6,
		verificationMaxRetries: 3,
		verificationWaitTime: 20 * 1000,
		sendPhoneVerificationCodeOnCreation: false
	}

	Email.onSend(function(err, res) {
		if(err){
			console.log(err);
		}
	});

	ServiceConfiguration.configurations.upsert({ 
		service: "facebook" 
	}, {
		$set: {
			appId: "721368684666472",
			loginStyle: "popup",
			secret: "a44b216a7271bcf667e076050126ba96"
		}
	});

	Accounts.onCreateUser(function(options, user) {
		
		userId = user._id;
		userPhone = user.username;

		var chat = {
			userIds: userId,
			createdAt: new Date()
		};

		var chatId = Chats.insert(chat);

		var	message = {
			text: "Hello there , thanks for using our app. If you need any help, just let me know. I'm available to answer any questions you may have.",
			type: "text",
			chatId: chatId,
			timestamp: new Date()
		};

		var messageId = Messages.insert(message);

		Chats.update(message.chatId, { $set: { lastMessage: message } });

		user.userDetails = { 
			'phone': { 
				number: userPhone, 
				verified: false	 
			}
		};

		return user;
	});
});

Meteor.methods({
	newMember: function (member) {
		let memberId = Accounts.createUser(member);
		Roles.addUsersToRoles(memberId, 'user');
		if(member.email) {
			Accounts.sendVerificationEmail(memberId);
		}
		return memberId;
	},
	verifyDetails: function (username) {
		var user = Accounts.findUserByUsername(username);

		if ( user && user.userDetails && user.userDetails.phone && user.userDetails.phone.number ) {
			if( _.isEqual(user.userDetails.phone.number, username) ) {
				if ( _.isEqual(user.userDetails.phone.verified, true) ) {
					return true;
				} else {
					throw new Meteor.Error('phone-not-verified', 'Phone Number is not verified.');
				}
			}  else {
				throw new Meteor.Error('phone-not-found', 'Phone Number not found.');
			}
		}  else {
			throw new Meteor.Error('user-not-valid', 'User is not valid.');
		}
	}
});