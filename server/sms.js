if (Meteor.isServer) { 
    Meteor.methods({
        otp: function (userPhone) {
            check(userPhone, String);

            const num = Random._randomString(6, '1234567890');

            const aid = '633070';
            const pin = 'car@3';
            let message = 'Hello,%0aA warm welcome!!!%0aVerify your phone number by typing OTP - ' + num + '.%0aPing your Personal Health Assistant right away.%0aTake Care.';

            let url = 'http://luna.a2wi.co.in:7501/failsafe/HttpLink?aid=' + aid + '&pin=' + pin + '&mnumber=91' + userPhone + '&message=' + message + ' &signature=CAREBD';
            
            var sms = {
                userPhone: userPhone,
                message: message,
                createdAt: new Date(),
                type: 'otp',
                status: {
                    content: 'Initialized'
                }
            };

            var smsId = SMS.insert(sms);

            this.unblock();

            HTTP.call("POST", url, function (error, result) {
                if (!error) {
                    SMS.update(smsId, {
                        $set: { 
                            'status.statusCode': result.statusCode, 
                            'status.content': result.content 
                        } 
                    });
                    if(result.statusCode == 200) {
                        ServerSession.set(userPhone, num);
                    } else {
                        throw new Meteor.Error(result.statusCode, result.content); 
                    }
                } else {
                    SMS.update(smsId, {
                        $set: {
                            'status.content': '',
                            'status.error': error
                        } 
                    });
                    return error;
                }
            });
        },
        verify: function (userPhone, otp) {
            check(userPhone, String);
            check(otp, String);

            if (ServerSession.equals(userPhone, otp, identical = true)) {
                var updateId = Meteor.users.update({ username: userPhone }, { 
                    $set: { 
                        'userDetails.phone.verified': true
                    } 
                });

                if(updateId) {
                    return true;
                } else {
                    return;
                }
            } else {
                return;
            }
        },
        newmemberText: function (userPhone) {
            check(userPhone, String);

            const aid = '633070';
            const pin = 'car@3';
            let message = 'Hello,%0aA warm welcome!!!%0aYou have been added as family member by. Please download app from http://market.android.com/search?q=co.carebuddy.app. Your login id and password is ' + userPhone;
            
            let url = 'http://luna.a2wi.co.in:7501/failsafe/HttpLink?aid=' + aid + '&pin=' + pin + '&mnumber=' + userPhone + '&message=' + message + ' &signature=CAREBD';
            
            var sms = {
                userPhone: userPhone,
                message: message,
                createdAt: new Date(),
                type: 'New member',
                status: {
                    content: 'Initialized'
                }
            };

            var smsId = SMS.insert(sms);

            this.unblock();
            
            HTTP.call("POST", url, function (error, result) {
                if (!error) {
                    SMS.update(smsId, {
                        $set: { 
                            'status.statusCode': result.statusCode, 
                            'status.content': result.content 
                        } 
                    });
                    if(result.statusCode == 200) {
                        return result;
                    } else {
                        throw new Meteor.Error(result.statusCode, result.content); 
                    }
                } else {
                    SMS.update(smsId, {
                        $set: {
                            'status.content': '',
                            'status.error': error
                        } 
                    });
                    return error;
                }
            });
        }
    });
}